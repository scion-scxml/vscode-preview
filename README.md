# SCXML Preview

Visual preview SCXML

![Preview](https://gitlab.com/scion-scxml/vscode-preview/raw/master/screenshot.gif?inline=true "Preview")

## Configuring your VSCode for SCXML development:

[![Link to video](https://img.youtube.com/vi/G7ADiXTP-LM/0.jpg)](https://www.youtube.com/watch?v=G7ADiXTP-LM)
